#define MYVERSION "3.1"

/*
	changelog

2022-02-12 11:43 UTC - kode54
- Update lazyusf2 with some important RSP HLE changes, which protect against out
  of bounds memory access. Fixes a crash with Conker's Bad Fur Day, among others.
- Disable RSP HLE warn logging. This was causing sparse41 and sparse43 from
  Conker's Bad Fur Day to return errors and fail to play with RSP HLE enabled.
- Get working with the new SDK
- Version is now 3.1

2020-12-28 01:20 UTC - kode54
- Updated lazyusf2 with fixes from upstream, thanks to JoshW. Fixes any number of
  USF sets having random pauses when looping for a while, due to an audio interrupt
  issue causing the emulator to loop for an entire range of the 32 bit system counter
- Update component to more recent SDK
- Version is now 3.0

2019-05-02 23:35 UTC - kode54
- Updated psflib to fix tag whitespace handling
- Version is now 2.3.4

2019-01-26 04:31 UTC - kode54
- Updated psflib, with verbose loading information
- Version is now 2.3.3

2018-12-01 20:18 UTC - kode54
- Fixed the sample rate being reported as 0Hz. I totally made a scope error there, wow.
- Version is now 2.3.2

2018-10-18 03:16 UTC - kode54
- Switched back to LazyUSF2. People can just put up with most old sets having
  playback errors. This probably solves the memory leak as well.
- Fixed some potential memory leaks in the player that would only affect
  out of memory conditions.
- Version is now 2.3.1

2018-07-22 00:10 UTC - kode54
- Switched back to the original LazyUSF, now with some enhancements added to it
  - Note, while LazyUSF is much slower, it's still sufficiently fast for many
    times real-time playback speed on most systems. And most rips would need
	to be re-ripped to work properly with LazyUSF2.
- Version is now 2.3

2018-01-30 09:09 UTC - kode54
- Updated to version 1.4 SDK
- Version is now 2.2.43

2017-11-02 23:20 UTC - kode54
- Update some other timing fixes from Josh W, which should fix some other rips.
- Version is now 2.2.42

2017-10-30 02:35 UTC - kode54
- Fix PJ64 save state loading with floating point in 32 bit mode
- Version is now 2.2.41

2017-02-04 04:31 UTC - kode54
- Enabled HLE audio by default, reset configuration GUID so it takes effect automatically
- Add link to about string
- Update links
- Version is now 2.2.40

2015-05-12 02:07 UTC - kode54
- Eliminated the buffer thread, as it was really flaky and also impeded
  conversion
- Report file sample rate to get_info queries, caching the result for
  subsequent calls
- Fixed seeking backwards, which was not resetting the known play position
- Version is now 2.2.39

2015-04-30 00:58 UTC - kode54
- Removed CPU lockup checker, users are now on their own if a set completely
  locks up their player
- Version is now 2.2.38

2015-04-30 00:36 UTC - kode54
- Shuffled around with compiler settings
- Changed CPU checkpoint code to try to prevent false alarms, like with
  Animal Crossing / Animal Forest
- Version is now 2.2.37

2015-04-20 05:20 UTC - kode54
- Fixed region setting in save state loader
- Version is now 2.2.36

2015-03-04 07:19 UTC - kode54
- Disabled TLB writes from triggering TLB Miss exceptions, which fixes
  Turok: Dinosaur Hunter, Turok 2: Seeds of Evil, and possibly others
- Version is now 2.2.35

2015-03-02 07:15 UTC - kode54
- Change resampler to a cubic resampler, based on RSP HLE / RSP microcode
- Version is now 2.2.34

2015-03-02 04:26 UTC - kode54
- Fixed the enablefifofull tag support in lazyusf2, which fixes Excitebike 64
- Version is now 2.2.33

2015-03-02 02:23 UTC - kode54
- Fixed the resample option checkbox
- Version is now 2.2.32

2015-02-28 08:32 UTC - kode54
- Fixed track restarting, which was crashing with the new library
- Support resampling to an arbitrary sample rate, enabled by default
- Version is now 2.2.31

2015-02-28 06:20 UTC - kode54
- Replaced original lazyusf with a newer Mupen64plus based lazyusf2
- Now properly checks for decoder errors from the buffer thread or container
  so errors will actually halt playback
- Version is now 2.2.30

2015-02-17 03:09 UTC - kode54
- Changed decoder to constantly refill the silence test buffer when not
  in real time playback
- Version is now 2.2.29

2015-02-16 06:26 UTC - kode54
- Change background thread mechanism a little bit, adding a 1.5 second pre-
  buffer, and adding a .5 second auto refill mechanism if the buffer runs
  empty, stopping if the thread can't keep up with real time
- Changed buffering to not use threading unless decoding is for real time
  playback
- Fixed buffer thread class retrieving samples from a filled buffer when
  there is no thread running
- Version is now 2.2.28

2015-01-16 04:16 UTC - kode54
- Fixed a crash by unifying parameter sizes to the buffer thread functions;
  the previous crashes were caused by passing remainder * 2 to the fill
  function, which already doubled the count itself, causing it to over-read
  into the input buffer
- Version is now 2.2.27

2014-12-21 07:01 UTC - kode54
- Fixed handling of _64thbug1/2 tags, which are summarily ignored, since
  this component always uses an interpreter R4300i anyway
- Version is now 2.2.26

2014-12-02 00:08 UTC - kode54
- Change buffer empty function so it doesn't busy loop when the buffer is
  completely empty
- Version is now 2.2.25

2014-11-30 05:26 UTC - kode54
- Whoops, forgot to regulate the buffer fill thread when the buffer becomes
  full, it was originally busy looping until there was space in the buffer
- Fixed up the R4300i HLE stuff a bit, should be faster now
- Re-profiled the optimization bits with Visual Studio
- Version is now 2.2.24

2014-11-29 07:52 UTC - kode54
- Implemented threaded buffering to alleviate some of the startup delays
- Version is now 2.2.23

2014-11-26 05:09 UTC - kode54
- Fixed parsing seconds/milliseconds only timestamps
- Version is now 2.2.22

2014-11-25 00:43 UTC - kode54
- Fixed _enablecompare tag reading into foobar2000 metadata
- Updated psflib to fix tag error handling
- Replaced timestamp parsing function
- Version is now 2.2.21

2014-11-24 08:36 UTC - kode54
- Updated lazyusf with a couple of fixes from Tom M., one for Banjo-Tooie
  and the other for Conker's Bad Fur Day
- Detect SSE2 support and if absent, force HLE audio, as that is compiled
  without SSE2 instructions, and likely to be necessary to run fast enough
  on such old processors
- Version is now 2.2.20

2014-11-23 08:50 UTC - kode54
- Implemented JoshW's old R4300i HLE functions, for a whopping 2% speedup
- Version is now 2.2.19

2014-10-23 02:12 UTC - kode54
- Updated LazyUSF again.
- HLE audio now supports most features, and in some cases is more compatible with
  some sets.
- Fixed TLB memory accesses when trying to read or write open bus, so they don't
  crash. This fixes Harvest Moon 64.
- Version is now 2.2.18

2014-04-08 00:15 UTC - kode54
- Updated LazyUSF again
- Changed default silence buffer size to 10 seconds, and changed option GUID to
  force the new default on all users; Fixes Majora's Mask - Staff Roll
- Version is now 2.2.17

 2014-04-06 03:25 UTC - kode54
- Added a safety break to the lazyusf execution loop, so it will error out
  on sets which are currently broken, such as Banjo-Tooie
- Version is now 2.2.16

2014-04-05 00:09 UTC - kode54
- Updated the RSP HLE audio code slightly
- Version is now 2.2.15

2014-04-03 01:41 UTC - kode54
- Updated lazyusf with even more fixes to the HLE audio code
- Version is now 2.2.14

2014-04-03 00:06 UTC - kode54
- Updated lazyusf with a fix to the HLE audio code
- Version is now 2.2.13

2014-03-27 22:28 UTC - kode54
- Fixed seeking when silence test buffer is not empty
- Version is now 2.2.12

2014-03-09 07:33 UTC - kode54
- Replaced preferences page and context menu GUIDs with unique ones
  so they work now
- Version is now 2.2.11

2014-03-09 07:19 UTC - kode54
- Fixed HLE audio checkbox triggering apply button
- Version is now 2.2.10

2014-03-09 01:37 UTC - kode54
- Updated to latest LazyUSF library
- Added option to enable HLE audio
- Version is now 2.2.9

2014-03-02 08:37 UTC - kode54
- Minor fixes
- Version is now 2.2.8

2014-02-27 00:28 UTC - kode54
- Now supports reading _enablecompare and _enablefifofull tags from
  nested library files and not just the topmost miniusf
- Version is now 2.2.7

2014-02-21 01:37 UTC - kode54
- Added error handling to emulation
- Fixed seeking backwards
- Version is now 2.2.6

2014-02-17 01:48 UTC - kode54
- Fixed inlining and various warnings
- Version is now 2.2.5

2014-02-16 08:10 UTC - kode54
- Bug fix
- Version is now 2.2.4

2014-02-16 07:26 UTC - kode54
- Fixed uninitialized variable
- Version is now 2.2.3

2014-02-16 07:16 UTC - kode54
- Fixed track ending after seeking
- Version is now 2.2.2

2014-02-16 07:08 UTC - kode54
- Oops. Fixed it so tracks actually end after fading out.
- Version is now 2.2.1

2014-02-16 06:17 UTC - kode54
- Ready for release
- Version is now 2.2

2014-02-16 04:46 UTC - kode54
- Copied from foo_input_gsf code base

*/

#include "../SDK/foobar2000.h"
#include "../helpers/window_placement_helper.h"
#include "../helpers/win32_dialog.h"

#include <atlbase.h>
#include <atlapp.h>
#include <atlctrls.h>
#include <atlctrlx.h>
#include <atlcrack.h>
#include <atlmisc.h>
#include "../helpers/atl-misc.h"
#include "../../libPPUI/wtl-pp.h"

#include "resource.h"

#include <stdio.h>

#include "../../../lazyusf/usf.h"

#include <psflib.h>

#include "circular_buffer.h"

//#define DBG(a) OutputDebugString(a)
#define DBG(a)

typedef unsigned long u_long;

critical_section g_sync;
static int initialized = 0;

// {4704B0D3-5695-42D1-9F20-0794DC5ADD21}
static const GUID guid_cfg_infinite = 
{ 0x4704b0d3, 0x5695, 0x42d1, { 0x9f, 0x20, 0x7, 0x94, 0xdc, 0x5a, 0xdd, 0x21 } };
// {492E2289-284E-4571-B13C-7174F118B840}
static const GUID guid_cfg_deflength = 
{ 0x492e2289, 0x284e, 0x4571, { 0xb1, 0x3c, 0x71, 0x74, 0xf1, 0x18, 0xb8, 0x40 } };
// {0E573114-A35E-45A6-88AF-9D7154510F36}
static const GUID guid_cfg_deffade = 
{ 0xe573114, 0xa35e, 0x45a6, { 0x88, 0xaf, 0x9d, 0x71, 0x54, 0x51, 0xf, 0x36 } };
// {24F5E4A9-2AE1-46B3-9A58-25623A1DFA1E}
static const GUID guid_cfg_suppressopeningsilence = 
{ 0x24f5e4a9, 0x2ae1, 0x46b3, { 0x9a, 0x58, 0x25, 0x62, 0x3a, 0x1d, 0xfa, 0x1e } };
// {E89022D7-87E9-4F78-9B60-6CA195500D55}
static const GUID guid_cfg_suppressendsilence = 
{ 0xe89022d7, 0x87e9, 0x4f78, { 0x9b, 0x60, 0x6c, 0xa1, 0x95, 0x50, 0xd, 0x55 } };
// {81DFCA33-64E4-4CD6-976C-D87BC7A3487C}
static const GUID guid_cfg_endsilenceseconds = 
{ 0x81dfca33, 0x64e4, 0x4cd6, { 0x97, 0x6c, 0xd8, 0x7b, 0xc7, 0xa3, 0x48, 0x7c } };
// {B6B4A385-7F56-4558-B630-78EB95280034}
static const GUID guid_cfg_placement = 
{ 0xb6b4a385, 0x7f56, 0x4558, { 0xb6, 0x30, 0x78, 0xeb, 0x95, 0x28, 0x0, 0x34 } };
// {A207BFF4-F44F-42AB-A35C-E0C511E8336F}
static const GUID guid_cfg_hle_audio =
{ 0xa207bff4, 0xf44f, 0x42ab,{ 0xa3, 0x5c, 0xe0, 0xc5, 0x11, 0xe8, 0x33, 0x6f } };
// {3BB2F74D-DFFB-4B46-89D4-9A4FDFC66D33}
static const GUID guid_cfg_resample =
{ 0x3bb2f74d, 0xdffb, 0x4b46, { 0x89, 0xd4, 0x9a, 0x4f, 0xdf, 0xc6, 0x6d, 0x33 } };
// {6A1F11B8-52B2-4133-B747-83F74BF27A3D}
static const GUID guid_cfg_resample_hz =
{ 0x6a1f11b8, 0x52b2, 0x4133, { 0xb7, 0x47, 0x83, 0xf7, 0x4b, 0xf2, 0x7a, 0x3d } };

enum
{
	default_cfg_infinite = 0,
	default_cfg_deflength = 170000,
	default_cfg_deffade = 10000,
	default_cfg_suppressopeningsilence = 1,
	default_cfg_suppressendsilence = 1,
	default_cfg_endsilenceseconds = 10,
	default_cfg_hle_audio = 1,
	default_cfg_resample = 1,
	default_cfg_resample_hz = 44100
};

static cfg_int cfg_infinite(guid_cfg_infinite,default_cfg_infinite);
static cfg_int cfg_deflength(guid_cfg_deflength,default_cfg_deflength);
static cfg_int cfg_deffade(guid_cfg_deffade,default_cfg_deffade);
static cfg_int cfg_suppressopeningsilence(guid_cfg_suppressopeningsilence,default_cfg_suppressopeningsilence);
static cfg_int cfg_suppressendsilence(guid_cfg_suppressendsilence,default_cfg_suppressendsilence);
static cfg_int cfg_endsilenceseconds(guid_cfg_endsilenceseconds,default_cfg_endsilenceseconds);
static cfg_int cfg_hle_audio(guid_cfg_hle_audio,default_cfg_hle_audio);
static cfg_int cfg_resample(guid_cfg_resample,default_cfg_resample);
static cfg_int cfg_resample_hz(guid_cfg_resample_hz,default_cfg_resample_hz);
static cfg_window_placement cfg_placement(guid_cfg_placement);

static const char field_length[]="usf_length";
static const char field_fade[]="usf_fade";

#if defined(_M_IX86) || defined(__i386__)

#ifdef _MSC_VER
#include <intrin.h>
#elif defined(__clang__) || defined(__GNUC__)
static inline void
__cpuid(int *data, int selector)
{
	asm("cpuid"
		: "=a" (data[0]),
		"=b" (data[1]),
		"=c" (data[2]),
		"=d" (data[3])
		: "a"(selector));
}
#else
#define __cpuid(a,b) memset((a), 0, sizeof(int) * 4)
#endif

static int query_cpu_feature_sse2() {
	int buffer[4];
	__cpuid(buffer, 1);
	if ((buffer[3] & (1 << 26)) == 0) return 0;
	return 1;
}

static int _usf_use_lle = query_cpu_feature_sse2();
#else
enum { _usf_use_lle = 1 }; // No x86, either x86_64 or no SSE2 compiled in anyway
#endif

#define BORK_TIME 0xC0CAC01A

static unsigned long parse_time_crap(const char *input)
{
	unsigned long value = 0;
	unsigned long multiplier = 1000;
	const char * ptr = input;
	unsigned long colon_count = 0;

	while (*ptr && ((*ptr >= '0' && *ptr <= '9') || *ptr == ':'))
	{
		colon_count += *ptr == ':';
		++ptr;
	}
	if (colon_count > 2) return BORK_TIME;
	if (*ptr && *ptr != '.' && *ptr != ',') return BORK_TIME;
	if (*ptr) ++ptr;
	while (*ptr && *ptr >= '0' && *ptr <= '9') ++ptr;
	if (*ptr) return BORK_TIME;

	ptr = strrchr(input, ':');
	if (!ptr)
		ptr = input;
	for (;;)
	{
		char * end;
		if (ptr != input) ++ptr;
		if (multiplier == 1000)
		{
			double temp = pfc::string_to_float(ptr);
			if (temp >= 60.0) return BORK_TIME;
			value = (long)(temp * 1000.0f);
		}
		else
		{
			unsigned long temp = strtoul(ptr, &end, 10);
			if (temp >= 60 && multiplier < 3600000) return BORK_TIME;
			value += temp * multiplier;
		}
		if (ptr == input) break;
		ptr -= 2;
		while (ptr > input && *ptr != ':') --ptr;
		multiplier *= 60;
	}

	return value;
}

static void print_time_crap(int ms, char *out)
{
	char frac[8];
	int i,h,m,s;
	if (ms % 1000)
	{
		sprintf(frac, ".%3.3d", ms % 1000);
		for (i = 3; i > 0; i--)
			if (frac[i] == '0') frac[i] = 0;
		if (!frac[1]) frac[0] = 0;
	}
	else
		frac[0] = 0;
	h = ms / (60*60*1000);
	m = (ms % (60*60*1000)) / (60*1000);
	s = (ms % (60*1000)) / 1000;
	if (h) sprintf(out, "%d:%2.2d:%2.2d%s",h,m,s,frac);
	else if (m) sprintf(out, "%d:%2.2d%s",m,s,frac);
	else sprintf(out, "%d%s",s,frac);
}

static void info_meta_add(file_info & info, const char * tag, pfc::ptr_list_t< const char > const& values)
{
	t_size count = info.meta_get_count_by_name( tag );
	if ( count )
	{
		// append as another line
		pfc::string8 final = info.meta_get(tag, count - 1);
		final += "\r\n";
		final += values[0];
		info.meta_modify_value( info.meta_find( tag ), count - 1, final );
	}
	else
	{
		info.meta_add(tag, values[0]);
	}
	for ( count = 1; count < values.get_count(); count++ )
	{
		info.meta_add( tag, values[count] );
	}
}

static void info_meta_ansi( file_info & info )
{
	for ( unsigned i = 0, j = info.meta_get_count(); i < j; i++ )
	{
		for ( unsigned k = 0, l = info.meta_enum_value_count( i ); k < l; k++ )
		{
			const char * value = info.meta_enum_value( i, k );
			info.meta_modify_value( i, k, pfc::stringcvt::string_utf8_from_ansi( value ) );
		}
	}
	for ( unsigned i = 0, j = info.info_get_count(); i < j; i++ )
	{
		const char * name = info.info_enum_name( i );
		if ( name[ 0 ] == '_' )
			info.info_set( pfc::string8( name ), pfc::stringcvt::string_utf8_from_ansi( info.info_enum_value( i ) ) );
	}
}

static int find_crlf(pfc::string8 & blah)
{
	int pos = blah.find_first('\r');
	if (pos >= 0 && *(blah.get_ptr()+pos+1) == '\n') return pos;
	return -1;
}

static const char * fields_to_split[] = {"ARTIST", "ALBUM ARTIST", "PRODUCER", "COMPOSER", "PERFORMER", "GENRE"};

static bool meta_split_value( const char * tag )
{
	for ( unsigned i = 0; i < _countof( fields_to_split ); i++ )
	{
		if ( !stricmp_utf8( tag, fields_to_split[ i ] ) ) return true;
	}
	return false;
}

static void info_meta_write(pfc::string_base & tag, const file_info & info, const char * name, int idx, int & first)
{
	pfc::string8 v = info.meta_enum_value(idx, 0);
	if (meta_split_value(name))
	{
		t_size count = info.meta_enum_value_count(idx);
		for (t_size i = 1; i < count; i++)
		{
			v += "; ";
			v += info.meta_enum_value(idx, i);
		}
	}

	int pos = find_crlf(v);

	if (pos == -1)
	{
		if (first) first = 0;
		else tag.add_byte('\n');
		tag += name;
		tag.add_byte('=');
		// r->write(v.c_str(), v.length());
		tag += v;
		return;
	}
	while (pos != -1)
	{
		pfc::string8 foo;
		foo = v;
		foo.truncate(pos);
		if (first) first = 0;
		else tag.add_byte('\n');
		tag += name;
		tag.add_byte('=');
		tag += foo;
		v = v.get_ptr() + pos + 2;
		pos = find_crlf(v);
	}
	if (v.length())
	{
		tag.add_byte('\n');
		tag += name;
		tag.add_byte('=');
		tag += v;
	}
}

struct psf_info_meta_state
{
	file_info * info;

	pfc::string8_fast name;

	bool utf8;

	int tag_song_ms;
	int tag_fade_ms;

	psf_info_meta_state()
		: info( 0 ), utf8( false ), tag_song_ms( 0 ), tag_fade_ms( 0 )
	{
	}
};

static int psf_info_meta(void * context, const char * name, const char * value)
{
	psf_info_meta_state * state = ( psf_info_meta_state * ) context;

	pfc::string8_fast & tag = state->name;

	tag = name;

	if (!stricmp_utf8(tag, "game"))
	{
		DBG("reading game as album");
		tag = "album";
	}
	else if (!stricmp_utf8(tag, "year"))
	{
		DBG("reading year as date");
		tag = "date";
	}

	if (!stricmp_utf8_partial(tag, "replaygain_"))
	{
		DBG("reading RG info");
		//info.info_set(tag, value);
		state->info->info_set_replaygain(tag, value);
	}
	else if (!stricmp_utf8(tag, "length"))
	{
		DBG("reading length");
		int temp = parse_time_crap(value);
		if (temp != BORK_TIME)
		{
			state->tag_song_ms = temp;
			state->info->info_set_int(field_length, state->tag_song_ms);
		}
	}
	else if (!stricmp_utf8(tag, "fade"))
	{
		DBG("reading fade");
		int temp = parse_time_crap(value);
		if (temp != BORK_TIME)
		{
			state->tag_fade_ms = temp;
			state->info->info_set_int(field_fade, state->tag_fade_ms);
		}
	}
	else if (!stricmp_utf8(tag, "utf8"))
	{
		state->utf8 = true;
	}
	else if (!stricmp_utf8_partial(tag, "_lib"))
	{
		DBG("found _lib");
		state->info->info_set(tag, value);
	}
	else if (!stricmp_utf8(tag, "_enablecompare"))
	{
		DBG("found _enablecompare");
		state->info->info_set(tag, value);
	}
	else if (!stricmp_utf8(tag, "_enablefifofull"))
	{
		DBG("found _enablefifofull");
		state->info->info_set(tag, value);
	}
	else if (!stricmp_utf8(tag, "_64thbug1") || !stricmp_utf8(tag, "_64thbug2"))
	{
		DBG("found _64thbug* tag, ignoring");
		state->info->info_set(tag, value);
	}
	else if (tag[0] == '_')
	{
		DBG("found unknown required tag, failing");
		console::formatter() << "Unsupported tag found: " << tag << ", required to play file.";
		return -1;
	}
	else
	{
		state->info->meta_add( tag, value );
	}

	return 0;
}

struct usf_loader_state
{
	uint32_t enable_compare;
	uint32_t enable_fifo_full;
	void * emu_state;

	usf_loader_state()
		: enable_compare(0), enable_fifo_full(0),
		  emu_state(0)
	{ }

	~usf_loader_state()
	{
		if ( emu_state )
			free( emu_state );
	}
};

int usf_loader(void * context, const uint8_t * exe, size_t exe_size,
                                  const uint8_t * reserved, size_t reserved_size)
{
	struct usf_loader_state * state = ( struct usf_loader_state * ) context;
    if ( exe_size > 0 ) return -1;

	return usf_upload_section( state->emu_state, reserved, reserved_size );
}

int usf_info(void * context, const char * name, const char * value)
{
	struct usf_loader_state * state = ( struct usf_loader_state * ) context;

	if ( pfc::stricmp_ascii( name, "_enablecompare" ) == 0 && strlen( value ) )
		state->enable_compare = 1;
	else if ( pfc::stricmp_ascii( name, "_enablefifofull" ) == 0 && strlen( value ) )
		state->enable_fifo_full = 1;

	return 0;
}

static class psf_file_container
{
	critical_section lock;

	struct psf_file_opened
	{
		pfc::string_simple path;
		file::ptr f;

		psf_file_opened() { }

		psf_file_opened( const char * _p )
			: path( _p ) { }

		psf_file_opened( const char * _p, file::ptr _f )
			: path( _p ), f( _f ) { }

		bool operator== ( psf_file_opened const& in ) const
		{
			return !strcmp( path, in.path );
		}
	};

	pfc::list_t<psf_file_opened> hints;

public:
	void add_hint( const char * path, file::ptr f )
	{
		insync( lock );
		hints.add_item( psf_file_opened( path, f ) );
	}

	void remove_hint( const char * path )
	{
		insync( lock );
		hints.remove_item( psf_file_opened( path ) );
	}

	bool try_hint( const char * path, file::ptr & out )
	{
		insync( lock );
		t_size index = hints.find_item( psf_file_opened( path ) );
		if ( index == ~0 ) return false;
		out = hints[ index ].f;
		out->reopen( abort_callback_dummy() );
		return true;
	}
} g_hint_list;

struct psf_file_state
{
	file::ptr f;
};

static void * psf_file_fopen( void * context, const char * uri )
{
	psf_file_state * state = 0;
	try
	{
		state = new psf_file_state;
		if ( !g_hint_list.try_hint( uri, state->f ) )
			filesystem::g_open( state->f, uri, filesystem::open_mode_read, abort_callback_dummy() );
		return state;
	}
	catch (...)
	{
		delete state;
		return NULL;
	}
}

static size_t psf_file_fread( void * buffer, size_t size, size_t count, void * handle )
{
	try
	{
		psf_file_state * state = ( psf_file_state * ) handle;
		size_t bytes_read = state->f->read( buffer, size * count, abort_callback_dummy() );
		return bytes_read / size;
	}
	catch (...)
	{
		return 0;
	}
}

static int psf_file_fseek( void * handle, int64_t offset, int whence )
{
	try
	{
		psf_file_state * state = ( psf_file_state * ) handle;
		state->f->seek_ex( offset, (foobar2000_io::file::t_seek_mode) whence, abort_callback_dummy() );
		return 0;
	}
	catch (...)
	{
		return -1;
	}
}

static int psf_file_fclose( void * handle )
{
	try
	{
		psf_file_state * state = ( psf_file_state * ) handle;
		delete state;
		return 0;
	}
	catch (...)
	{
		return -1;
	}
}

static long psf_file_ftell( void * handle )
{
	try
	{
		psf_file_state * state = ( psf_file_state * ) handle;
		return state->f->get_position( abort_callback_dummy() );
	}
	catch (...)
	{
		return -1;
	}
}

const psf_file_callbacks psf_file_system =
{
	"\\/|:",
	NULL,
	psf_file_fopen,
	psf_file_fread,
	psf_file_fseek,
	psf_file_fclose,
	psf_file_ftell
};

static void print_message(void * context, const char * message)
{
	pfc::string8 * msgbuf = (pfc::string8 *)context;
	msgbuf->add_string(message);
}

class input_usf : public input_stubs
{
	bool no_loop, eof, in_playback, resample;

	pfc::array_t<t_int16> sample_buffer;

	usf_loader_state * m_state;

	service_ptr_t<file> m_file;

	pfc::string8 m_path;

	int32_t sample_rate, last_sample_rate;

	int err;

	int data_written,remainder;
	
	double startsilence,silence;

	int song_len,fade_len;
	int tag_song_ms,tag_fade_ms;

	file_info_impl m_info;

	bool do_suppressendsilence;

	circular_buffer<t_int16> m_buffer;

public:
	input_usf() : m_state(0)
	{
	}

	~input_usf()
	{
		g_hint_list.remove_hint( m_path );

		shutdown();
	}

	void shutdown()
	{
		if ( m_state )
		{
			usf_shutdown( m_state->emu_state );
			delete m_state;
			m_state = 0;
		}
	}

	void open( service_ptr_t<file> p_file, const char * p_path, t_input_open_reason p_reason, abort_callback & p_abort )
	{
		input_open_file_helper( p_file, p_path, p_reason, p_abort );

		m_path = p_path;
		g_hint_list.add_hint( p_path, p_file );

		psf_info_meta_state info_state;
		info_state.info = &m_info;

		pfc::string8_fast msgbuf;

		int ret = psf_load(p_path, &psf_file_system, 0x21, 0, 0, psf_info_meta, &info_state, 0, print_message, &msgbuf);

		console::print(msgbuf);
		msgbuf.reset();

		if ( ret <= 0 )
			throw exception_io_data( "Not a USF file" );

		if ( !info_state.utf8 )
			info_meta_ansi( m_info );

		tag_song_ms = info_state.tag_song_ms;
		tag_fade_ms = info_state.tag_fade_ms;

		if (!tag_song_ms)
		{
			tag_song_ms = cfg_deflength;
			tag_fade_ms = cfg_deffade;
		}

		m_info.set_length( (double)( tag_song_ms + tag_fade_ms ) * .001 );
		m_info.info_set_int( "channels", 2 );

		m_file = p_file;
	}

	void get_info( file_info & p_info, abort_callback & p_abort )
	{
		const char * srate_text = m_info.info_get("samplerate");
		if (!srate_text)
		{
			if (cfg_resample)
				m_info.info_set_int("samplerate", cfg_resample_hz);
			else
			{
				int32_t sample_rate = 0;
				usf_loader_state * p_state = 0;
				try
				{
					p_state = new usf_loader_state;

					p_state->emu_state = malloc(usf_get_state_size());

					if (!p_state->emu_state)
						throw std::bad_alloc();

					usf_clear(p_state->emu_state);

					usf_set_hle_audio(p_state->emu_state, !_usf_use_lle || cfg_hle_audio);

					pfc::string8_fast msgbuf;

					int ret = psf_load(m_path, &psf_file_system, 0x21, usf_loader, p_state, usf_info, p_state, 1, print_message, &msgbuf);

					console::print(msgbuf);
					msgbuf.reset();

					if (ret < 0)
						throw exception_io_data("Invalid USF");

					usf_set_compare(p_state->emu_state, p_state->enable_compare);
					usf_set_fifo_full(p_state->emu_state, p_state->enable_fifo_full);

					const char * err = usf_render(p_state->emu_state, 0, 0, &sample_rate);
					if (err != 0)
					{
						console::print(err);
						throw exception_io_data();
					}

					usf_shutdown(p_state->emu_state);
					delete p_state;
					p_state = 0;
				}
				catch (std::exception & e)
				{
					delete p_state;
					throw e;
				}

				m_info.info_set_int("samplerate", sample_rate);
			}
		}
		p_info.copy(m_info);
	}

	t_filestats get_file_stats( abort_callback & p_abort )
	{
		return m_file->get_stats( p_abort );
	}

	void decode_initialize( unsigned p_flags, abort_callback & p_abort )
	{
		shutdown();

		m_state = new usf_loader_state;

		m_state->emu_state = malloc( usf_get_state_size() );

		if (!m_state->emu_state)
			throw std::bad_alloc();

		usf_clear( m_state->emu_state );

		usf_set_hle_audio( m_state->emu_state, !_usf_use_lle || cfg_hle_audio );

		pfc::string8_fast msgbuf;

		int ret = psf_load(m_path, &psf_file_system, 0x21, usf_loader, m_state, usf_info, m_state, 1, print_message, &msgbuf);

		console::print(msgbuf);
		msgbuf.reset();

		if ( ret < 0 )
			throw exception_io_data( "Invalid USF" );

		usf_set_compare( m_state->emu_state, m_state->enable_compare );
		usf_set_fifo_full( m_state->emu_state, m_state->enable_fifo_full );

		startsilence = silence = 0;

		eof = 0;
		err = 0;
		data_written = 0;
		remainder = 0;
		no_loop = ( p_flags & input_flag_no_looping ) || !cfg_infinite;
		resample = cfg_resample;

		sample_rate = resample ? cfg_resample_hz : 0;
		last_sample_rate = 0;

		sample_buffer.grow_size( 1024 * 2 );

		do_suppressendsilence = !! cfg_suppressendsilence;

		in_playback = !!( p_flags & input_flag_playback );

		double skip_max = double(cfg_endsilenceseconds);

		if ( cfg_suppressopeningsilence ) // ohcrap
		{
			for (;;)
			{
				p_abort.check();

				unsigned skip_howmany = 1024;
				const char * err = resample ? usf_render_resampled( m_state->emu_state, sample_buffer.get_ptr(), skip_howmany, sample_rate )
					: usf_render( m_state->emu_state, sample_buffer.get_ptr(), skip_howmany, &sample_rate );
				if ( err != 0 )
					throw exception_io_data( err );
				short * foo = ( short * ) sample_buffer.get_ptr();
				unsigned i;
				for ( i = 0; i < skip_howmany; ++i )
				{
					if ( foo[ 0 ] || foo[ 1 ] ) break;
					foo += 2;
				}
				silence += double(i) / double(sample_rate);
				if ( i < skip_howmany )
				{
					remainder = skip_howmany - i;
					memmove( sample_buffer.get_ptr(), foo, remainder * sizeof( short ) * 2 );
					break;
				}
				if ( silence >= skip_max )
				{
					eof = true;
					break;
				}
			}

			startsilence += silence;
			silence = 0;
		}

		if ( do_suppressendsilence )
		{
			if ( !resample && !sample_rate )
			{
				const char * err = usf_render( m_state->emu_state, 0, 0, &sample_rate );
				if ( err != 0 )
					throw exception_io_data( err );
			}

			m_buffer.resize(sample_rate * skip_max * 2);
			if (remainder)
			{
				unsigned long count;
				t_int16 * ptr = m_buffer.get_write_ptr(count);
				memcpy(ptr, sample_buffer.get_ptr(), remainder * sizeof(t_int16) * 2);
				m_buffer.samples_written(remainder * 2);
				remainder = 0;
			}
		}

		if ( sample_rate )
			calcfade();
	}

	bool decode_run( audio_chunk & p_chunk, abort_callback & p_abort )
	{
		p_abort.check();

		if ( eof || err < 0 ) return false;

		if ( no_loop && tag_song_ms && sample_rate && data_written >= (song_len + fade_len) )
			return false;

		UINT written = 0;

		int samples = 1024;

		short * ptr;

		if ( do_suppressendsilence )
		{
			unsigned long max_fill = no_loop ? ((song_len + fade_len) - data_written) * 2 : 0;

			unsigned long done = 0;
			unsigned long count;
			ptr = m_buffer.get_write_ptr(count);
			if (max_fill && count > max_fill)
				count = max_fill;

			count /= 2;

			while (count)
			{
				p_abort.check();
				unsigned int todo = 1024;
				if (todo > count)
					todo = (unsigned int)count;
				const char * err = resample ? usf_render_resampled(m_state->emu_state, ptr, todo, sample_rate) :
					usf_render(m_state->emu_state, ptr, todo, &sample_rate);
				if (err != 0)
				{
					console::print(err);
					throw exception_io_data();
				}
				ptr += todo * 2;
				done += todo;
				count -= todo;
			}

			m_buffer.samples_written(done * 2);

			if ( m_buffer.test_silence() )
			{
				eof = true;
				return false;
			}

			written = m_buffer.data_available() / 2;
			if (written > 1024)
				written = 1024;
			sample_buffer.grow_size( 1024 * 2 );
			m_buffer.read(sample_buffer.get_ptr(), written * 2);
			ptr = sample_buffer.get_ptr();
		}
		else
		{
			if ( remainder )
			{
				written = remainder;
				remainder = 0;
			}
			else
			{
				const char * err = resample ? usf_render_resampled( m_state->emu_state, sample_buffer.get_ptr(), 1024, sample_rate )
					: usf_render( m_state->emu_state, sample_buffer.get_ptr(), 1024, &sample_rate );
				if ( err != 0 )
					throw exception_io_data( err );
				written = 1024;
			}

			ptr = (short *) sample_buffer.get_ptr();
		}

		int d_start, d_end;
		d_start = data_written;
		data_written += written;
		d_end = data_written;

		calcfade();

		if ( tag_song_ms && d_end > song_len && no_loop )
		{
			short * foo = sample_buffer.get_ptr();
			int n;
			for( n = d_start; n < d_end; ++n )
			{
				if ( n > song_len )
				{
					if ( n > song_len + fade_len )
					{
						* ( DWORD * ) foo = 0;
					}
					else
					{
						int bleh = song_len + fade_len - n;
						foo[ 0 ] = MulDiv( foo[ 0 ], bleh, fade_len );
						foo[ 1 ] = MulDiv( foo[ 1 ], bleh, fade_len );
					}
				}
				foo += 2;
			}
		}

		p_chunk.set_data_fixedpoint( ptr, written * 4, sample_rate, 2, 16, audio_chunk::channel_config_stereo );

		return true;
	}

	void decode_seek( double p_seconds, abort_callback & p_abort )
	{
		eof = false;

		double usfemu_pos = 0.0;

		if ( sample_rate )
		{
			usfemu_pos = double(data_written) / double(sample_rate);

			if ( do_suppressendsilence )
			{
				double buffered_time = (double)(m_buffer.data_available() / 2) / (double)(sample_rate);

				m_buffer.reset();

				usfemu_pos += buffered_time;
			}
		}

		last_sample_rate = 0;

		if ( p_seconds < usfemu_pos )
		{
			usf_restart( m_state->emu_state );
			usfemu_pos = -startsilence;
			data_written = 0;
		}

		p_seconds -= usfemu_pos;

		// more abortable, and emu doesn't like doing huge numbers of samples per call anyway
		while ( p_seconds )
		{
			p_abort.check();

			const char * err = resample ? usf_render_resampled( m_state->emu_state, sample_buffer.get_ptr(), 1024, sample_rate )
				: usf_render( m_state->emu_state, sample_buffer.get_ptr(), 1024, &sample_rate );
			if ( err != 0 )
				throw exception_io_data( err );

			usfemu_pos += 1024.0 / double(sample_rate);

			data_written += 1024;

			p_seconds -= 1024.0 / double(sample_rate);

			if ( p_seconds < 0 )
			{
				remainder = (unsigned int)(-p_seconds * double(sample_rate));

				data_written -= remainder;

				memmove( sample_buffer.get_ptr(), &sample_buffer[ ( 1024 - remainder ) * 2 ], remainder * 2 * sizeof(int16_t) );

				break;
			}
		}

		if (do_suppressendsilence && remainder)
		{
			unsigned long count;
			t_int16 * ptr = m_buffer.get_write_ptr(count);
			memcpy(ptr, sample_buffer.get_ptr(), remainder * sizeof(t_int16) * 2);
			m_buffer.samples_written(remainder * 2);
			remainder = 0;
		}
	}

	bool decode_can_seek()
	{
		return true;
	}

	bool decode_get_dynamic_info( file_info & p_out, double & p_timestamp_delta )
	{
		if ( sample_rate != last_sample_rate )
		{
			last_sample_rate = sample_rate;
			p_out.info_set_int( "samplerate", sample_rate );
			p_timestamp_delta = 0;
			return true;
		}

		return false;
	}

	bool decode_get_dynamic_info_track( file_info & p_out, double & p_timestamp_delta )
	{
		return false;
	}

	void decode_on_idle( abort_callback & p_abort )
	{
	}

	void retag( const file_info & p_info, abort_callback & p_abort )
	{
		m_info.copy( p_info );

		pfc::array_t<t_uint8> buffer;
		buffer.set_size( 16 );

		m_file->seek( 0, p_abort );

		BYTE *ptr = buffer.get_ptr();
		m_file->read_object( ptr, 16, p_abort );
		if (ptr[0] != 'P' || ptr[1] != 'S' || ptr[2] != 'F' ||
			ptr[3] != 0x21) throw exception_io_data();
		int reserved_size = pfc::byteswap_if_be_t( ((unsigned long*)ptr)[1] );
		int exe_size = pfc::byteswap_if_be_t( ((unsigned long*)ptr)[2] );
		m_file->seek(16 + reserved_size + exe_size, p_abort);
		m_file->set_eof(p_abort);

		pfc::string8 tag = "[TAG]utf8=1\n";

		int first = 1;
		// _lib and _refresh tags first
		int n, p = p_info.info_get_count();
		for (n = 0; n < p; n++)
		{
			const char *t = p_info.info_enum_name(n);
			if (*t == '_')
			{
				if (first) first = 0;
				else tag.add_byte('\n');
				tag += t;
				tag.add_byte('=');
				tag += p_info.info_enum_value(n);
			}
		}
		// Then info
		p = p_info.meta_get_count();
		for (n = 0; n < p; n++)
		{
			const char * t = p_info.meta_enum_name(n);
			if (*t == '_' ||
				!stricmp(t, "length") ||
				!stricmp(t, "fade")) continue; // dummy protection
			if (!stricmp(t, "album")) info_meta_write(tag, p_info, "game", n, first);
			else if (!stricmp(t, "date"))
			{
				const char * val = p_info.meta_enum_value(n, 0);
				char * end;
				strtoul(p_info.meta_enum_value(n, 0), &end, 10);
				if (size_t(end - val) < strlen(val))
					info_meta_write(tag, p_info, t, n, first);
				else
					info_meta_write(tag, p_info, "year", n, first);
			}
			else info_meta_write(tag, p_info, t, n, first);
		}
		// Then time and fade
		{
			int tag_song_ms = 0, tag_fade_ms = 0;
			const char *t = p_info.info_get(field_length);
			if (t)
			{
				char temp[16];
				tag_song_ms = atoi(t);
				if (first) first = 0;
				else tag.add_byte('\n');
				tag += "length=";
				print_time_crap(tag_song_ms, temp);
				tag += temp;
				t = p_info.info_get(field_fade);
				if (t)
				{
					tag_fade_ms = atoi(t);
					tag.add_byte('\n');
					tag += "fade=";
					print_time_crap(tag_fade_ms, (char *)&temp);
					tag += temp;
				}
			}
		}

		// Then ReplayGain
		/*
		p = p_info.info_get_count();
		for (n = 0; n < p; n++)
		{
			const char *t = p_info.info_enum_name(n);
			if (!strnicmp(t, "replaygain_",11))
			{
				if (first) first = 0;
				else tag.add_byte('\n');
				tag += t;
				else tag.add_byte('=');
				tag += p_info.info_enum_value(n);
			}
		}
		*/
		replaygain_info rg = p_info.get_replaygain();
		char rgbuf[replaygain_info::text_buffer_size];
		if (rg.is_track_gain_present())
		{
			rg.format_track_gain(rgbuf);
			if (first) first = 0;
			else tag.add_byte('\n');
			tag += "replaygain_track_gain";
			tag.add_byte('=');
			tag += rgbuf;
		}
		if (rg.is_track_peak_present())
		{
			rg.format_track_peak(rgbuf);
			if (first) first = 0;
			else tag.add_byte('\n');
			tag += "replaygain_track_peak";
			tag.add_byte('=');
			tag += rgbuf;
		}
		if (rg.is_album_gain_present())
		{
			rg.format_album_gain(rgbuf);
			if (first) first = 0;
			else tag.add_byte('\n');
			tag += "replaygain_album_gain";
			tag.add_byte('=');
			tag += rgbuf;
		}
		if (rg.is_album_peak_present())
		{
			rg.format_album_peak(rgbuf);
			if (first) first = 0;
			else tag.add_byte('\n');
			tag += "replaygain_album_peak";
			tag.add_byte('=');
			tag += rgbuf;
		}

		m_file->write_object( tag.get_ptr(), tag.length(), p_abort );
	}

	void remove_tags(abort_callback & p_abort)
	{
		file_info_impl temp(m_info);
		temp.meta_remove_all();
		temp.reset_replaygain();
		retag(temp, p_abort);
	}

	static bool g_is_our_content_type( const char * p_content_type )
	{
		return false;
	}

	static bool g_is_our_path( const char * p_full_path, const char * p_extension )
	{
		return (!stricmp(p_extension,"usf") || !stricmp(p_extension,"miniusf"));
	}

	static GUID g_get_guid()
	{
		return { 0x9b1ccec3, 0x1da6, 0x40f9,{ 0x93, 0x33, 0x56, 0x42, 0x9d, 0xe6, 0xd9, 0x60 } };
	}

	static const char * g_get_name()
	{
		return "USF Decoder";
	}

	static GUID g_get_preferences_guid()
	{
		return { 0x7aad88ee, 0x38eb, 0x456c,{ 0xb7, 0x29, 0x50, 0x1, 0x2, 0x16, 0x64, 0xa0 } };
	}

private:
	void calcfade()
	{
		song_len=MulDiv(tag_song_ms,sample_rate,1000);
		fade_len=MulDiv(tag_fade_ms,sample_rate,1000);
	}
};

class CMyPreferences : public CDialogImpl<CMyPreferences>, public preferences_page_instance {
public:
	//Constructor - invoked by preferences_page_impl helpers - don't do Create() in here, preferences_page_impl does this for us
	CMyPreferences(preferences_page_callback::ptr callback) : m_callback(callback) {}

	//Note that we don't bother doing anything regarding destruction of our class.
	//The host ensures that our dialog is destroyed first, then the last reference to our preferences_page_instance object is released, causing our object to be deleted.


	//dialog resource ID
	enum {IDD = IDD_PSF_CONFIG};
	// preferences_page_instance methods (not all of them - get_wnd() is supplied by preferences_page_impl helpers)
	t_uint32 get_state();
	void apply();
	void reset();

	//WTL message map
	BEGIN_MSG_MAP(CMyPreferences)
		MSG_WM_INITDIALOG(OnInitDialog)
		COMMAND_HANDLER_EX(IDC_INDEFINITE, BN_CLICKED, OnButtonClick)
		COMMAND_HANDLER_EX(IDC_SOS, BN_CLICKED, OnButtonClick)
		COMMAND_HANDLER_EX(IDC_SES, BN_CLICKED, OnButtonClick)
		COMMAND_HANDLER_EX(IDC_HLE_AUDIO, BN_CLICKED, OnButtonClick)
		COMMAND_HANDLER_EX(IDC_RESAMPLE, BN_CLICKED, OnButtonClick)
		COMMAND_HANDLER_EX(IDC_SILENCE, EN_CHANGE, OnEditChange)
		COMMAND_HANDLER_EX(IDC_DLENGTH, EN_CHANGE, OnEditChange)
		COMMAND_HANDLER_EX(IDC_RESAMPLE_HZ, EN_CHANGE, OnEditChange)
		COMMAND_HANDLER_EX(IDC_DFADE, EN_CHANGE, OnEditChange)
	END_MSG_MAP()
private:
	BOOL OnInitDialog(CWindow, LPARAM);
	void OnEditChange(UINT, int, CWindow);
	void OnButtonClick(UINT, int, CWindow);
	bool HasChanged();
	void OnChanged();

	const preferences_page_callback::ptr m_callback;

	CHyperLink m_link_github;
	CHyperLink m_link_kode54;
};

BOOL CMyPreferences::OnInitDialog(CWindow, LPARAM) {
	SendDlgItemMessage( IDC_INDEFINITE, BM_SETCHECK, cfg_infinite );
	SendDlgItemMessage( IDC_SOS, BM_SETCHECK, cfg_suppressopeningsilence );
	SendDlgItemMessage( IDC_SES, BM_SETCHECK, cfg_suppressendsilence );
	SendDlgItemMessage( IDC_HLE_AUDIO, BM_SETCHECK, cfg_hle_audio || !_usf_use_lle );
	SendDlgItemMessage( IDC_RESAMPLE, BM_SETCHECK, cfg_resample );
	GetDlgItem( IDC_HLE_AUDIO ).EnableWindow( _usf_use_lle );
	
	SetDlgItemInt( IDC_SILENCE, cfg_endsilenceseconds, FALSE );
	SetDlgItemInt( IDC_RESAMPLE_HZ, cfg_resample_hz, FALSE );
	
	{
		char temp[16];
		// wsprintf((char *)&temp, "= %d Hz", 33868800 / cfg_divider);
		// SetDlgItemText(wnd, IDC_HZ, (char *)&temp);
		
		print_time_crap( cfg_deflength, (char *)&temp );
		uSetDlgItemText( m_hWnd, IDC_DLENGTH, (char *)&temp );
		
		print_time_crap( cfg_deffade, (char *)&temp );
		uSetDlgItemText( m_hWnd, IDC_DFADE, (char *)&temp );
	}
	
	m_link_github.SetLabel( _T( "lazyusf git repository" ) );
	m_link_github.SetHyperLink( _T( "https://gitlab.kode54.net/kode54/lazyusf" ) );
	m_link_github.SubclassWindow( GetDlgItem( IDC_URL ) );
	
	m_link_kode54.SetLabel( _T( "kode's foobar2000 plug-ins" ) );
	m_link_kode54.SetHyperLink( _T( "https://kode54.net/fb2k/" ) );
	m_link_kode54.SubclassWindow( GetDlgItem( IDC_K54 ) );
	
	{
		/*OSVERSIONINFO ovi = { 0 };
		ovi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
		BOOL bRet = ::GetVersionEx(&ovi);
		if ( bRet && ( ovi.dwMajorVersion >= 5 ) )*/
		{
			DWORD color = GetSysColor( 26 /* COLOR_HOTLIGHT */ );
			m_link_github.m_clrLink = color;
			m_link_github.m_clrVisited = color;
			m_link_kode54.m_clrLink = color;
			m_link_kode54.m_clrVisited = color;
		}
	}
	
	return FALSE;
}

void CMyPreferences::OnEditChange(UINT, int, CWindow) {
	OnChanged();
}

void CMyPreferences::OnButtonClick(UINT, int, CWindow) {
	OnChanged();
}

t_uint32 CMyPreferences::get_state() {
	t_uint32 state = preferences_state::resettable;
	if (HasChanged()) state |= preferences_state::changed;
	return state;
}

void CMyPreferences::reset() {
	char temp[16];
	SendDlgItemMessage( IDC_INDEFINITE, BM_SETCHECK, default_cfg_infinite );
	SendDlgItemMessage( IDC_SOS, BM_SETCHECK, default_cfg_suppressopeningsilence );
	SendDlgItemMessage( IDC_SES, BM_SETCHECK, default_cfg_suppressendsilence );
	SendDlgItemMessage( IDC_RESAMPLE, BM_SETCHECK, default_cfg_resample );
	if ( _usf_use_lle )
		SendDlgItemMessage(IDC_HLE_AUDIO, BM_SETCHECK, default_cfg_hle_audio);
	SetDlgItemInt( IDC_SILENCE, default_cfg_endsilenceseconds, FALSE );
	SetDlgItemInt( IDC_RESAMPLE_HZ, default_cfg_resample_hz, FALSE );
	print_time_crap(default_cfg_deflength, (char *)&temp);
	uSetDlgItemText( m_hWnd, IDC_DLENGTH, (char *)&temp );
	print_time_crap( default_cfg_deffade, (char *)&temp );
	uSetDlgItemText( m_hWnd, IDC_DFADE, (char *)&temp );
	
	OnChanged();
}

void CMyPreferences::apply() {
	int t;
	char temp[16];
	cfg_infinite = SendDlgItemMessage( IDC_INDEFINITE, BM_GETCHECK );
	cfg_suppressopeningsilence = SendDlgItemMessage( IDC_SOS, BM_GETCHECK );
	cfg_suppressendsilence = SendDlgItemMessage( IDC_SES, BM_GETCHECK );
	if ( _usf_use_lle )
		cfg_hle_audio = SendDlgItemMessage( IDC_HLE_AUDIO, BM_GETCHECK );
	cfg_resample = SendDlgItemMessage( IDC_RESAMPLE, BM_GETCHECK );
	t = GetDlgItemInt( IDC_SILENCE, NULL, FALSE );
	if ( t > 0 ) cfg_endsilenceseconds = t;
	t = GetDlgItemInt( IDC_RESAMPLE_HZ, NULL, FALSE );
	if (t < 8000) t = 8000;
	else if (t > 192000) t = 192000;
	cfg_resample_hz = t;
	SetDlgItemInt( IDC_RESAMPLE_HZ, t, FALSE );
	SetDlgItemInt( IDC_SILENCE, cfg_endsilenceseconds, FALSE );
	t = parse_time_crap( string_utf8_from_window( GetDlgItem( IDC_DLENGTH ) ) );
	if ( t != BORK_TIME ) cfg_deflength = t;
	else
	{
		print_time_crap( cfg_deflength, (char *)&temp );
		uSetDlgItemText( m_hWnd, IDC_DLENGTH, (char *)&temp );
	}
	t = parse_time_crap( string_utf8_from_window( GetDlgItem( IDC_DFADE ) ) );
	if ( t != BORK_TIME ) cfg_deffade = t;
	else
	{
		print_time_crap( cfg_deffade, (char *)&temp );
		uSetDlgItemText( m_hWnd, IDC_DFADE, (char *)&temp );
	}
	
	OnChanged(); //our dialog content has not changed but the flags have - our currently shown values now match the settings so the apply button can be disabled
}

bool CMyPreferences::HasChanged() {
	//returns whether our dialog content is different from the current configuration (whether the apply button should be enabled or not)
	bool changed = false;
	if ( !changed && SendDlgItemMessage( IDC_INDEFINITE, BM_GETCHECK ) != cfg_infinite ) changed = true;
	if ( !changed && SendDlgItemMessage( IDC_SOS, BM_GETCHECK ) != cfg_suppressopeningsilence ) changed = true;
	if ( !changed && SendDlgItemMessage( IDC_SES, BM_GETCHECK ) != cfg_suppressendsilence ) changed = true;
	if ( _usf_use_lle && !changed && SendDlgItemMessage( IDC_HLE_AUDIO, BM_GETCHECK ) != cfg_hle_audio ) changed = true;
	if ( !changed && SendDlgItemMessage( IDC_RESAMPLE, BM_GETCHECK ) != cfg_resample ) changed = true;
	if ( !changed && GetDlgItemInt( IDC_SILENCE, NULL, FALSE ) != cfg_endsilenceseconds ) changed = true;
	if ( !changed && GetDlgItemInt( IDC_RESAMPLE_HZ, NULL, FALSE ) != cfg_resample_hz ) changed = true;
	if ( !changed )
	{
		int t = parse_time_crap( string_utf8_from_window( GetDlgItem( IDC_DLENGTH ) ) );
		if ( t != BORK_TIME && t != cfg_deflength ) changed = true;
	}
	if ( !changed )
	{
		int t = parse_time_crap( string_utf8_from_window( GetDlgItem( IDC_DFADE ) ) );
		if ( t != BORK_TIME && t != cfg_deffade ) changed = true;
	}
	return changed;
}
void CMyPreferences::OnChanged() {
	//tell the host that our state has changed to enable/disable the apply button appropriately.
	m_callback->on_state_changed();
}

class preferences_page_myimpl : public preferences_page_impl<CMyPreferences> {
	// preferences_page_impl<> helper deals with instantiation of our dialog; inherits from preferences_page_v3.
public:
	const char * get_name() {return input_usf::g_get_name();}
	GUID get_guid() {return input_usf::g_get_preferences_guid();}
	GUID get_parent_guid() {return guid_input;}
};

typedef struct
{
	unsigned song, fade;
} INFOSTRUCT;

static BOOL CALLBACK TimeProc(HWND wnd,UINT msg,WPARAM wp,LPARAM lp)
{
	switch(msg)
	{
	case WM_INITDIALOG:
		uSetWindowLong(wnd,DWL_USER,lp);
		{
			INFOSTRUCT * i=(INFOSTRUCT*)lp;
			char temp[16];
			if (!i->song && !i->fade) uSetWindowText(wnd, "Set length");
			else uSetWindowText(wnd, "Edit length");
			if ( i->song != ~0 )
			{
				print_time_crap(i->song, (char*)&temp);
				uSetDlgItemText(wnd, IDC_LENGTH, (char*)&temp);
			}
			if ( i->fade != ~0 )
			{
				print_time_crap(i->fade, (char*)&temp);
				uSetDlgItemText(wnd, IDC_FADE, (char*)&temp);
			}
		}
		cfg_placement.on_window_creation(wnd);
		return 1;
	case WM_COMMAND:
		switch(wp)
		{
		case IDOK:
			{
				INFOSTRUCT * i=(INFOSTRUCT*)uGetWindowLong(wnd,DWL_USER);
				int foo;
				foo = parse_time_crap(string_utf8_from_window(wnd, IDC_LENGTH));
				if (foo != BORK_TIME) i->song = foo;
				else i->song = ~0;
				foo = parse_time_crap(string_utf8_from_window(wnd, IDC_FADE));
				if (foo != BORK_TIME) i->fade = foo;
				else i->fade = ~0;
			}
			EndDialog(wnd,1);
			break;
		case IDCANCEL:
			EndDialog(wnd,0);
			break;
		}
		break;
	case WM_DESTROY:
		cfg_placement.on_window_destruction(wnd);
		break;
	}
	return 0;
}

static bool context_time_dialog(unsigned * song_ms, unsigned * fade_ms)
{
	bool ret;
	INFOSTRUCT * i = new INFOSTRUCT;
	if (!i) return 0;
	i->song = *song_ms;
	i->fade = *fade_ms;
	HWND hwnd = core_api::get_main_window();
	ret = uDialogBox(IDD_TIME, hwnd, TimeProc, (long)i) > 0;
	if (ret)
	{
		*song_ms = i->song;
		*fade_ms = i->fade;
	}
	delete i;
	return ret;
}

class length_info_filter : public file_info_filter
{
	bool set_length, set_fade;
	unsigned m_length, m_fade;

	metadb_handle_list m_handles;

public:
	length_info_filter( const pfc::list_base_const_t<metadb_handle_ptr> & p_list )
	{
		set_length = false;
		set_fade = false;

		pfc::array_t<t_size> order;
		order.set_size(p_list.get_count());
		order_helper::g_fill(order.get_ptr(),order.get_size());
		p_list.sort_get_permutation_t(pfc::compare_t<metadb_handle_ptr,metadb_handle_ptr>,order.get_ptr());
		m_handles.set_count(order.get_size());
		for(t_size n = 0; n < order.get_size(); n++) {
			m_handles[n] = p_list[order[n]];
		}

	}

	void length( unsigned p_length )
	{
		set_length = true;
		m_length = p_length;
	}

	void fade( unsigned p_fade )
	{
		set_fade = true;
		m_fade = p_fade;
	}

	virtual bool apply_filter(metadb_handle_ptr p_location,t_filestats p_stats,file_info & p_info)
	{
		t_size index;
		if (m_handles.bsearch_t(pfc::compare_t<metadb_handle_ptr,metadb_handle_ptr>,p_location,index))
		{
			if ( set_length )
			{
				if ( m_length ) p_info.info_set_int( field_length, m_length );
				else p_info.info_remove( field_length );
			}
			if ( set_fade )
			{
				if ( m_fade ) p_info.info_set_int( field_fade, m_fade );
				else p_info.info_remove( field_fade );
			}
			return set_length | set_fade;
		}
		else
		{
			return false;
		}
	}
};

class context_usf : public contextmenu_item_simple
{
public:
	virtual unsigned get_num_items() { return 1; }

	virtual void get_item_name(unsigned n, pfc::string_base & out)
	{
		if (n) uBugCheck();
		out = "Edit length";
	}

	/*virtual void get_item_default_path(unsigned n, pfc::string_base & out)
	{
		out.reset();
	}*/
	GUID get_parent() {return contextmenu_groups::tagging;}

	virtual bool get_item_description(unsigned n, pfc::string_base & out)
	{
		if (n) uBugCheck();
		out = "Edits the length of the selected USF file, or sets the length of all selected USF files.";
		return true;
	}

	virtual GUID get_item_guid(unsigned p_index)
	{
		if (p_index) uBugCheck();
		static const GUID guid = { 0x8d0b14b6, 0xe331, 0x404a, { 0x90, 0x4f, 0xdd, 0x10, 0x3a, 0x5a, 0x47, 0xb0 } };
		return guid;
	}

	virtual bool context_get_display(unsigned n,const pfc::list_base_const_t<metadb_handle_ptr> & data,pfc::string_base & out,unsigned & displayflags,const GUID &)
	{
		if (n) uBugCheck();
		unsigned i, j;
		i = data.get_count();
		for (j = 0; j < i; j++)
		{
			pfc::string8 ext = pfc::string_extension(data.get_item(j)->get_path());
			if (pfc::stricmp_ascii(ext, "USF") && pfc::stricmp_ascii(ext, "MINIUSF")) return false;
		}
		if (i == 1) out = "Edit length";
		else out = "Set length";
		return true;
	}

	virtual void context_command(unsigned n,const pfc::list_base_const_t<metadb_handle_ptr> & data,const GUID& caller)
	{
		if (n) uBugCheck();
		unsigned tag_song_ms = 0, tag_fade_ms = 0;
		unsigned i = data.get_count();
		file_info_impl info;
		abort_callback_impl m_abort;
		if (i == 1)
		{
			// fetch info from single file
			metadb_handle_ptr handle = data.get_item(0);
			handle->metadb_lock();
			const file_info * p_info;
			if (handle->get_info_locked(p_info) && p_info)
			{
				const char *t = p_info->info_get(field_length);
				if (t) tag_song_ms = atoi(t);
				t = p_info->info_get(field_fade);
				if (t) tag_fade_ms = atoi(t);
			}
			handle->metadb_unlock();
		}
		if (!context_time_dialog(&tag_song_ms, &tag_fade_ms)) return;
		static_api_ptr_t<metadb_io_v2> p_imgr;

		service_ptr_t<length_info_filter> p_filter = new service_impl_t< length_info_filter >( data );
		if ( tag_song_ms != ~0 ) p_filter->length( tag_song_ms );
		if ( tag_fade_ms != ~0 ) p_filter->fade( tag_fade_ms );

		p_imgr->update_info_async( data, p_filter, core_api::get_main_window(), 0, 0 );
	}
};

class version_usf : public componentversion
{
public:
	virtual void get_file_name(pfc::string_base & out) { out = core_api::get_my_file_name(); }
	virtual void get_component_name(pfc::string_base & out) { out = "LazyUSF"; }
	virtual void get_component_version(pfc::string_base & out) { out = MYVERSION; }
	virtual void get_about_message(pfc::string_base & out)
	{
		out = "Foobar2000 version by kode54\nOriginal library by hcs64 and JoshW." /*"\n\nCore: ";
		out += psx_getversion();
		out +=*/ "\n\nhttps://gitlab.kode54.net/kode54/lazyusf\nhttps://kode54.net/fb2k/\nhttps://www.patreon.com/kode54";
	}
};

DECLARE_FILE_TYPE( "USF files", "*.USF;*.MINIUSF" );

static input_singletrack_factory_t<input_usf>               g_input_usf_factory;
static preferences_page_factory_t <preferences_page_myimpl> g_config_usf_factory;
static contextmenu_item_factory_t <context_usf>             g_contextmenu_item_usf_factory;
static service_factory_single_t   <version_usf>             g_componentversion_usf_factory;

VALIDATE_COMPONENT_FILENAME("foo_input_usf.dll");
